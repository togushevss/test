import { getUsers } from './Model';

export const queries = {
    users: (_, __, context) => {
        if(context.req.username) {
            return getUsers()
        } else {
            return getUsers()
                .then(res => res.map(({ name, username }) => ({
                    name,
                    username,
                    password: null
                })))
        }
    }
}