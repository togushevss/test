import { _logIn, _register } from './Model';

export const mutations = {
    register:(_, {  registerInput } ) => _register(registerInput),
    logIn: (_, { username, password }, context) => _logIn(username, password, context)
}