import { getDeviceById, getDevices, getDevicesByUser } from './Model';

export const queries = {
    devices: () => getDevices(),
    device: (_, { id }) => getDeviceById(id),
    deviceByUser: (_, { owner }) => getDevicesByUser(owner)

}