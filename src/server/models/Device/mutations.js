import { _addDevice, _deleteDevice, _updateDevice } from './Model'

export const mutations = {
    addDevice: (_, { device }) => _addDevice(device),
    updateDevice: (_, { id, device }) => _updateDevice(id, device),
    deleteDevice: (_, { id }) => _deleteDevice(id),
}