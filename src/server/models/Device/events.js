export const events = Object.freeze({
    DEVICE_ADDED: 'DEVICE_ADDED',
    DEVICE_UPDATE: 'DEVICE_UPDATE',
    DEVICE_DELETE: 'DEVICE_DELETE'
})