// Core
import { pubSub } from '../../init/server'

// Events
import { events } from './events'

export const subscriptions = {
    device: {
        subscribe: () => pubSub.asyncIterator([events.DEVICE_ADDED, events.DEVICE_UPDATE, events.DEVICE_DELETE])
    }
}