// Core
import { useQuery } from '@apollo/react-hooks'
import { loader } from 'graphql.macro'

// Queries
const queryDevicesByUser = loader('./gql/queryDevicesByUser.graphql')

export const useQueryDevicesByUser = (owner) => {

    const { loading, error, data } = useQuery(queryDevicesByUser, {
        variables: {
            owner
        }
    })

    return { loading, error, devices: data && data.deviceByUser }
}