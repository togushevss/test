// Core
import React from 'react'
import { useFormik } from 'formik'

// Mutations
import { useMutationLogin } from './hooks/useMutationLogin'

// MaterialUI
import { makeStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'

const useStyles = makeStyles({
    root: {
        width: "30%",
        margin: "0 auto",
        padding: 15
    },
    flex: {
        display: 'flex',
        flexDirection: 'column'
    },
    button: {
        width: "50%",
        marginTop: "10px",
        alignSelf: "center"
    }
})

export const LoginForm = () => {
    const classes = useStyles()

    const formik = useFormik({
        initialValues: {
            username: '',
            password: ''
        },
        onSubmit: values => {
            login()
        }
    })

    const { login } = useMutationLogin(formik.values)

    return (
        <form className={classes.root} onSubmit={formik.handleSubmit}>
            <div className={classes.flex}>
                <TextField
                    required
                    id="username"
                    label="Имя пользователя"
                    onChange={formik.handleChange}
                    value={formik.values.username}
                />
                <TextField
                    required
                    type="password"
                    id="password"
                    label="Пароль"
                    onChange={formik.handleChange}
                    value={formik.values.password}
                />
                <Button className={classes.button} type='submit' variant="contained" color="secondary">
                    Войти
                </Button>
            </div>
        </form>
    )
}