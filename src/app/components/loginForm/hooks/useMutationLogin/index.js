// Core
import { useContext } from 'react';
import { useMutation } from '@apollo/react-hooks'
import { loader } from 'graphql.macro'
import { AuthContext } from '../../../../utils/context/AuthContext';

// Mutations
const MutationLogin = loader('./gql/MutationLogin.graphql')

export const useMutationLogin = ({ username, password }) => {
    const context = useContext(AuthContext)

    const [ login, { data } ] = useMutation(MutationLogin, {
        update(_, { data: { logIn: userData }}) {
            context.login(userData)
        },
        onError: (data) => {
            console.log('Неправильные данные')
        },
        variables: {
            username, password
        }
    })

    return { login, createData: data }
}